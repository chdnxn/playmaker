const {Howl, Howler} = require('howler');
const { Subject } = require('rxjs');

module.exports = class PlaylistItem {

    constructor(song, artist, approved, needsEdit) {
        this._song = song;
        this._artist = artist;
        this._approved = approved || false;
        this._needsEdit = needsEdit || false;
        this._player = new Howl({
            src: [song.split("#").join("%23")],
            preload: false
        });
        this._duration = 0;
        this._hour = 0;
        this._minute = 0;
        this._second = 0;
        
        /**
         *
         * @type {Subject}
         * @private
         */
        this._timeString = new Subject();

        this._timeString.subscribe(x => {
            //console.log(x);
        });
    }

    get song() {
        return this._song;
    }

    set song(value) {
        this._song = value;
    }

    get artist() {
        return this._artist;
    }

    set artist(value) {
        this._artist = value;
    }

    get approved() {
        return this._approved;
    }

    set approved(value) {
        this._approved = value;
    }

    get needsEdit() {
        return this._needsEdit;
    }

    set needsEdit(value) {
        this._needsEdit = value;
    }

    get player() {
        return this._player;
    }

    get duration() {
        return this._duration;
    }

    set duration(duration) {
        this._duration = duration;
    }

    play = () => {
        const play = this;
        play.player.load();
        play.player.once('load', function(){
            play.duration = play.player.duration();
            play.player.play();
            let playing  = setInterval(function() {
                if (play.player.playing()) {
                    play.setTime(play.player.seek()); // time in seconds
                    $('button.time').text([play.hour, play.minute, play.second].join(':'));

                    play.timeString.next([play.hour, play.minute, play.second].join(':'));
                } else {
                    clearInterval(playing);
                }
            }, 100);
        });
    }

    playOnChromecast = () => {
        this.player.play();
    }
    
    get hour() {
        return this._hour;
    }

    set hour(hour) {
        this._hour = hour;
    }
    
    get minute() {
        return this._minute;
    }

    set minute(minute) {
        this._minute = minute;
    }
    
    get second() {
        return this._second;
    }

    set second(second) {
        this._second = second;
    }

    /**
     *
     * @returns {Subject}
     */
    get timeString() {
        return this._timeString;
    }

    /**
     *
     * @param {Subject} timeString
     */
    set timeString(timeString) {
        this._timeString = timeString;
    }

    setTime(e){
        this.hour = Math.floor(e / 3600).toString().padStart(2,'0'),
        this.minute = Math.floor(e % 3600 / 60).toString().padStart(2,'0'),
        this.second = Math.floor(e % 60).toString().padStart(2,'0');      
    }

}