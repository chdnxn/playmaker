const $ = require('jquery');
const PlaylistUtils = require('./playlist-utils.js');

let playlistUtils = new PlaylistUtils();


const pick = function (track) {
    playlistUtils.pick(track);
};

const play = function (played) {
    playlistUtils.play(played);
};

const load = function (track) {
    playlistUtils.load(track);
};

const loadSongFromPC = function (track) {
    playlistUtils.loadSongFromPC(track);
};

const upOne = function (track) {
    playlistUtils.upOne(track);
};

const downOne = function (played) {
    playlistUtils.downOne(played);
};

const anotherFromThisArtist = function (item) {
    playlistUtils.anotherFromThisArtist(item);
}

const approve = function (item) {
    playlistUtils.approve(item);
}

const needsEdit = function (item) {
    playlistUtils.needsEdit(item);
}

const loadNew = function () {
    playlistUtils.eraseCurrent();
    playlistUtils.getNewPlaylist();
}

playlistUtils.getNewPlaylist();







