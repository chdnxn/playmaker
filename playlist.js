const parser = require("xml2js");
const PlaylistItem = require('./playlistItem.js');

const directoryPath = 'E:\\Music\\';
const variousDir = directoryPath + "VA\\";
const playlistDir = 'C:\\Users\\Chad Nixon\\Music\\Playlists\\';
const currentFile = playlistDir + "current\\current.wpl";

module.exports = class Playlist {

    constructor() {
        this._artists = this.getArtists().then(function(){
            /**
             * @type {PlaylistItem[]} list
             * @private
             */
            this._list = [];
            
        });
        /**
         * @type {PlaylistItem[]} list
         * @private
         */
        this._list = [];
        /**
         *
         * @type {number}
         * @private
         */
        this._index = 0;
        this._currentSong = {};
    }

    set list(list) {
        this._list = list;
    }

    get list() {
        return this._list
    }

    /**
     *
     * @returns {PlaylistItem} currentSong
     */
    get currentSong() {
        return this._currentSong;
    }

    /**
     *
     * @param {PlaylistItem} song
     */
    set currentSong(song) {
        this._currentSong = song;
        this.play();
    }

    getNewPlaylist() {
        let getNew = this;
        return new Promise((resolve, reject) => {
            fs.stat(currentFile, function (error, stats) {
                if (!error && stats.isFile()) {
                    getNew.loadFromFile(currentFile);
                    if (!artists.length) {
                        getNew.getArtists();
                    }
                } else {
                    if (!artists.length) {
                        getNew.getArtists().then(() => {
                            playlist = getNew.recurse(0, artists, playlist);
                            getNew.populateFrontend();
                        });
                    } else {
                        playlist = getNew.recurse(0, artists, playlist);
                        getNew.populateFrontend();
                    }
                }
            });
        });
    }

    getNewPlaylist2 = () => {

        client.on('device', function(device){
            $('#devices').append('<option value="' + device.name + '" >' + device.friendlyName + '</option>');
        });

        // setTimeout(function() {
        //     client.devices.forEach(function(device) {
        //         console.log(device);
        //         $('#devices').append('<option value="' + device.name + '" >' + device.friendlyName + '</option>');
        //     });
        // }, 3000)



    }

    loadSongFromPC = (item) => {
        item = $(item);
        let artist = item.parents('li').attr('data-artist');
        let loadSong = this;
        toRemove.clear();

        let response = dialog.showOpenDialog({
            properties: ['openFile'],
            defaultPath: artist,
            filters: [
                {name: 'Audio File', extensions: ['mp3', 'flac']}
            ]
        });

        response.then((promise) => {
            playlist.delete(artist);
            if (promise.filePaths.length) {
                let file = promise.filePaths[0];
                let chunks = file.split('\\');
                let newArtist = [chunks[0], chunks[1], chunks[2]].join('\\') + '\\';
                playlist.set(newArtist, new PlaylistItem(file, newArtist, false, false));
                loadSong.populateFrontend();
            }
        });
    }

    loadFromFile(filePath) {
        let load = this;
        let xmlString = fs.readFileSync(filePath, "utf8");
        parser.parseString(xmlString.replace('/\\/g', '\\\\'), function (error, result) {
            if (error === null) {
                let list = result.smil.body[0].seq[0].media;
                playlist.clear();
                list.forEach(function (song) {
                    let chunks = song.ATTR.src.split('\\');
                    let artist = ([chunks[0], chunks[1], chunks[2]].join('\\') + '\\').replace('/\\/g', '\\\\');
                    let newSong = song.ATTR.src.replace('/\\/g', '\\\\');
                    let approved = song.ATTR.approved === 'true';
                    let needsEdit = song.ATTR.needsEdit === 'true';
                    if (artist === 'E:\\Music\\VA\\') {
                        artist = artist + Math.random();
                        playlist.set(artist , new PlaylistItem(newSong, artist, approved, needsEdit));
                        load.populateFrontend();
                    } else {
                        playlist.set(artist, new PlaylistItem(newSong, artist, approved, needsEdit));
                        load.populateFrontend();
                    }
                });

            } else {
                console.log(error);
            }
        });
    }

    getArtists() {

        return new Promise((resolve, reject) => {
            fs.readdir(directoryPath, function (err, files) {
                if (err) {
                    reject(err);
                }

                files.forEach(function (file) {
                    if (fs.statSync(directoryPath + file).isDirectory()) {
                        artists.push(directoryPath + file + '\\');
                    }
                });

                resolve();
            });
        });
    }

    walkSync = (dir, filelist) => {
        let walksync = this;
        let files = fs.readdirSync(dir);
        filelist = filelist || [];

        files.forEach(function (file) {
            if (fs.statSync(dir + file).isDirectory()) {
                filelist = walksync.walkSync(dir + file + '\\', filelist);
            } else {
                if (path.extname(file) === '.mp3' || path.extname(file) === '.flac') {
                    filelist.push(dir + file);
                }
            }
        });
        return filelist;
    };

    recurse = (length, artists, playlist) => {
        if (!length) {
            length = 0;
        }

        if (length > 35) {
            return playlist;
        }

        let randomArtist = artists[Math.floor(Math.random() * artists.length)];
        if (Math.random() > .9) {
            randomArtist = variousDir;
        }

        if (randomArtist && (!playlist.has(randomArtist) || randomArtist.endsWith("\\VA\\"))) {
            let artistSongs = this.walkSync(randomArtist, []);
            let randomSong = artistSongs[Math.floor(Math.random() * artistSongs.length)];

            if (randomArtist.endsWith("\\VA\\")) {
                randomArtist += Math.random() + "";
            }

            if (randomSong) {
                playlist.set(randomArtist, new PlaylistItem(randomSong, randomArtist, false, false));
                length++;
            }
        }
        return this.recurse(length, artists, playlist);

    }

    putPlaylistFiles = () => {
        const time = Date.now();
        const finalDir = playlistDir + "final\\";
        let thisPlaylistDir = finalDir + time + "_playlist\\";
        let thisPlaylistFile = thisPlaylistDir + time + '_playlist.wpl';
        fs.mkdir(thisPlaylistDir, function (err) {
            if (err) {
                console.log(err)
            } else {
                let count = 0;
                playlist.forEach((value, key) => {
                    let countString = count + "";
                    if (value && value.song) {
                        fs.copyFile(value.song, finalDir + time + "_playlist\\" + countString.padStart(3, '0') + "_" + path.basename(value.song), (err) => {
                            if (err) throw err;
                        });
                    }
                    count++;
                });
                fs.copyFile(currentFile, thisPlaylistFile, (err) => {
                    if (err) throw err;
                });
            }
        })
    }

    exportPlaylist = (isCurrent) => {
        const time = Date.now();
        let exportList = this;
        const top = '<?wpl version="1.0"?>\n' +
            '<smil>\n' +
            '    <head>\n' +
            '        <meta name="Generator" content="Microsoft Windows Media Player -- 12.0.18362.900"/>\n' +
            '        <meta name="ItemCount" content="1"/>\n' +
            '        <title>test</title>\n' +
            '    </head>\n' +
            '    <body>\n' +
            '        <seq>\n';
        const bottom = '        </seq>\n' +
            '    </body>\n' +
            '</smil>';

        let lines = '';
        let thisPlaylistDir = playlistDir + time + "_playlist\\";
        let thisPlaylistFile = thisPlaylistDir + time + '_playlist.wpl';

        if (isCurrent) {
            thisPlaylistFile = currentFile;
        }


        playlist.forEach((value, key) => {
            if (value && value.song) {
                lines += '            <media needsEdit="' + value.needsEdit + '" approved="' + value.approved + '" src="' + XmlEntities.encode(value.song) + '"/>\n';
            }
        });

        if (!isCurrent) {
            fs.mkdirSync(thisPlaylistDir);
            fs.writeFile(thisPlaylistFile, top + lines + bottom, 'utf-8', function (err) {
                if (err) {
                    return console.log(err);
                }
                //exportList.soundplay(thisPlaylistFile);
            });
        } else {
            fs.writeFile(thisPlaylistFile, top + lines + bottom, 'utf-8', function (err) {
                if (err) {
                    return console.log(err);
                }
            });
        }
    }

    getNextSong = (song) => {
        let next = false;
        let nextSong = '';
        playlist.forEach((value, key) => {
            if (next) {
                nextSong = value.song;
                next = false;
            } else {
                if (value.song === song) {
                    next = true;
                }
            }
        });

        return nextSong;
    }

    removeAndRefill = () => {
        toRemove.forEach(function (value, key) {
            playlist.delete(key);
        });

        playlist = this.recurse(playlist.size, artists, playlist);

        this.populateFrontend();
        toRemove.clear();
    }

    generatePlaylistFile = () => {
        this.exportPlaylist();
    }

    upOne = (item) => {
        item = $(item);
        toRemove.clear();
        let artist = item.parents('li').attr('data-artist');
        let song = playlist.get(artist);
        let lastOneKey = null;
        let lastOneValue = null;
        let toSwitchWithKey = null;
        let toSwitchWithValue = null

        playlist.forEach(function (value, key) {
            if (key === artist) {
                toSwitchWithKey = lastOneKey;
                toSwitchWithValue = lastOneValue;
            }
            lastOneKey = key;
            lastOneValue = value;
        });

        if (toSwitchWithKey) {
            let keys = Array.from(playlist.keys());
            let values = Array.from(playlist.values());
            let switchThisTime = false;

            playlist.clear();

            for (let i = 0; i < keys.length; i++) {

                let k = keys[i];
                let v = values[i];
                if (k === toSwitchWithKey) {
                    k = artist;
                    v = song;
                    switchThisTime = true;
                } else {
                    if (switchThisTime) {
                        k = toSwitchWithKey;
                        v = toSwitchWithValue;
                        switchThisTime = false;
                    }
                }

                playlist.set(k, v);

            }
            this.populateFrontend();
        }
    }

    downOne = (item) => {
        item = $(item);
        toRemove.clear();
        let artist = item.parents('li').attr('data-artist');
        let song = playlist.get(artist);
        let lastOneKey = null;
        let lastOneValue = null;
        let toSwitchWithKey = null;
        let toSwitchWithValue = null

        playlist.forEach(function (value, key) {
            if (lastOneKey === artist) {
                toSwitchWithKey = key;
                toSwitchWithValue = value;
            }
            lastOneKey = key;
            lastOneValue = value;
        });
        if (toSwitchWithKey) {
            let keys = Array.from(playlist.keys());
            let values = Array.from(playlist.values());
            let switchThisTime = false;

            playlist.clear();

            for (let i = 0; i < keys.length; i++) {

                let k = keys[i];
                let v = values[i];
                if (k === artist) {
                    k = toSwitchWithKey;
                    v = toSwitchWithValue;
                    switchThisTime = true;
                } else {
                    if (switchThisTime) {
                        k = artist;
                        v = song;
                        switchThisTime = false;
                    }
                }

                playlist.set(k, v);

            }
            this.populateFrontend();
        }
    }

    anotherFromThisArtist = (item) => {
        item = $(item);
        const variousDir = "E:\\Music\\VA\\";
        let artist = item.parents('li').attr('data-artist');
        let artistOriginal = artist;
        if (artist.startsWith(variousDir)) {
            artist = variousDir;
        }
        let artistSongs = this.walkSync(artist, []);
        let randomSong = artistSongs[Math.floor(Math.random() * artistSongs.length)];
        playlist.set(artistOriginal, new PlaylistItem(randomSong, artistOriginal, false, false));
        this.populateFrontend();
    }

    pick = (track) => {
        let item = $(track);
        let artist = item.parents('li').attr('data-artist');
        if (!toRemove.has(artist)) {
            toRemove.set(artist, item.text());
            $(track).parents('li').css('background', 'linear-gradient(90deg, rgb(121, 136, 212) 0%, rgb(170, 191, 214) 35%, rgb(252, 255, 227) 100%)');
        } else {
            $(track).parents('li').css('background', 'linear-gradient(90deg, rgb(121, 136, 212) 0%, rgb(206, 206, 255) 35%, rgb(242, 242, 245) 100%)');
            toRemove.delete(artist);
        }
    };
}