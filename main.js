const {app, BrowserWindow} = require('electron')
const path = require('path');



function createWindow() {
    // Create the browser window.
    const win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true
        }
    })

    // and load the index.html of the app.
    win.loadFile('index.html')

    // Open the DevTools.
     // win.webContents.openDevTools()


}

app.allowRendererProcessReuse = false;


// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(createWindow)

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow()
    }
})

const express = require('express')();
const http = require('http').Server(express);
const music = '/music/';
const image = '/image/';
express.get(music + '*', (req, res)=> {
    if (req.originalUrl.startsWith(music)) {
        let tail = req.originalUrl.split(music)[1];
        res.sendFile('E:\\\\Music\\' + decodeURI(tail.split('/').join( '\\').split("%23").join("#")));
    }

    if (req.originalUrl.startsWith(image)) {
        res.sendFile('http://www.palindromemusic.com/palindrome.jpg');
    }
});

http.listen(3000, '192.168.1.76', function () {
    console.log('listening on port 3000');
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

