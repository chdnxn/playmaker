const {Howl, Howler} = require('howler');
const PlaylistItem = require('./playlistItem.js');
const { Subject, Subscription } = require('rxjs');

/**
 * It plays songs.
 *
 * @type {Player}
 */
module.exports = class Player {
    /**
     *  Constructor.
     *
     * @param {PlaylistItem[]} queue
     */
    constructor(queue) {
        this._queue = queue;
        /**
         * @private {Subscription<PlaylistItem>} _currentSong
         */
        this._currentSong = new Subject(queue[0]);
        /**
         * @private {Subscription<number>} _currentIndex
         */
        this._currentIndex = new Subject(0);
        /**
         * @private {Subscription<string>} _currentSongTime
         */
        this._currentSongTime = new Subject('00:00:00')
        /**
         * @private {Subscription<string>} _currentSongLength
         */
        this._currentSongLength = new Subject('00:00:00');
        /**
         * @private {Subject<number>} _currentSongTimeSeconds
         */
        this._currentSongTimeSeconds = new Subject(0);
        /**
         * @private {Subject<number>} _currentSongLengthSeconds
         */
        this._currentSongLengthSeconds = new Subject(0);
    }

    /**
     *
     * @returns {Subject<number>} currentSongLengthSeconds
     */
    get currentSongLengthSeconds() {
        return this._currentSongLengthSeconds;
    }

    /**
     *
     * @param {number} seconds
     */
    set currentSongLengthSeconds(seconds) {
        this._currentSongLengthSeconds = index;
    }

    /**
     *
     * @returns {Subscription<number>} currentSong
     */
    get currentSongTimeSeconds() {
        return this._currentSongTimeSeconds;
    }

    /**
     *
     * @param {Subscription<number>} index
     */
    set currentSongTimeSeconds(index) {
        this._currentSongTimeSeconds = index;
    }
    
    /**
     *
     * @returns {Subscription<string>} currentSong
     */
    get currentSongLength() {
        return this._currentSongLength;
    }

    /**
     *
     * @param {Subscription<string>} index
     */
    set currentSongLength(index) {
        this._currentSongLength = index;
    }

    /**
     *
     * @returns {Subscription<string>} currentSong
     */
    get currentSongTime() {
        return this._currentSongTime;
    }

    /**
     *
     * @param {Subscription<string>} index
     */
    set currentSongTime(index) {
        this._currentSongTime = index;
    }

    /**
     *
     * @returns {Subscription<number>} currentSong
     */
    get currentIndex() {
        return this._currentIndex;
    }

    /**
     *
     * @param {Subscription<number>} index
     */
    set currentIndex(index) {
        this._currentIndex = index;
    }

    /**
     *
     * @param {number} [i]
     */
    setCurrentSongByIndex(i) {
        this.currentIndex = i || 0;
        this.currentSong = i ? this.queue[i]: this.queue[0];
        this.play();
    }

    nextSong() {
        this.setCurrentSongByIndex(this.currentIndex++);
    }

    previousSong() {
        this.setCurrentSongByIndex(this.currentIndex--);
    }

    /**
     *
     * @returns {Array} queue
     */
    get queue() {
        return this._queue;
    }

    /**
     *
     * @param {Array} queue
     */
    set queue(queue) {
        this._queue = queue;
    }

    /**
     *
     * @param {number} [i]
     */
    play(i) {
        if (i) {
            this.currentSong = this.queue[i];
        }
        this.currentSong.play(); 
    }
}