/**
 * User: Chad Nixon
 * Date: 8/9/2020
 * Time: 12:17 AM
 */

const {dialog} = require('electron').remote
const fs = require('fs');
const {XmlEntities} = require('html-entities');
const {exec} = require('child_process')
const execPromise = require('util').promisify(exec)
const path = require('path');
const $ = require('jquery');
const xml2js = require('xml2js');
const parser = new xml2js.Parser({attrkey: "ATTR"});
const PlaylistItem = require('./playlistItem.js');
const {Howl, Howler} = require('howler');

let playlist = new Map();
let toRemove = new Map();
let artists = [];
const directoryPath = 'E:\\Music\\';
const playlistDir = 'C:\\Users\\Chad Nixon\\Music\\Playlists\\';
const finalDir = 'C:\\Users\\Chad Nixon\\Music\\Playlists\\final\\';
let currentFile = playlistDir + "current\\current.wpl";

const ChromecastAPI = require('chromecast-api');

const client = new ChromecastAPI()

module.exports = class PlaylistUtils {
    pick = (track) => {
        let item = $(track);
        let artist = item.parents('li').attr('data-artist');
        if (!toRemove.has(artist)) {
            toRemove.set(artist, item.text());
            $(track).parents('li').css('background', 'linear-gradient(90deg, rgb(121, 136, 212) 0%, rgb(170, 191, 214) 35%, rgb(252, 255, 227) 100%)');
        } else {
            $(track).parents('li').css('background', 'linear-gradient(90deg, rgb(121, 136, 212) 0%, rgb(206, 206, 255) 35%, rgb(242, 242, 245) 100%)');
            toRemove.delete(artist);
        }
    };

    play = (played) => {
        let item = $(played);
        let song = item.parents('li').text();
        this.soundplay(song);
    }

    anotherFromThisArtist = (item) => {
        item = $(item);
        let artist = item.parents('li').attr('data-artist');

        let artistSongs = this.walkSync(artist, []);
        let randomSong = artistSongs[Math.floor(Math.random() * artistSongs.length)];
        playlist.set(artist, new PlaylistItem(randomSong, artist, false, false));
        this.populateFrontend();
    }

    upOne = (item) => {
        item = $(item);
        toRemove.clear();
        let artist = item.parents('li').attr('data-artist');
        let song = playlist.get(artist);
        let lastOneKey = null;
        let lastOneValue = null;
        let toSwitchWithKey = null;
        let toSwitchWithValue = null

        playlist.forEach(function (value, key) {
            if (key === artist) {
                toSwitchWithKey = lastOneKey;
                toSwitchWithValue = lastOneValue;
            }
            lastOneKey = key;
            lastOneValue = value;
        });

        if (toSwitchWithKey) {
            let keys = Array.from(playlist.keys());
            let values = Array.from(playlist.values());
            let switchThisTime = false;

            playlist.clear();

            for (let i = 0; i < keys.length; i++) {

                let k = keys[i];
                let v = values[i];
                if (k === toSwitchWithKey) {
                    k = artist;
                    v = song;
                    switchThisTime = true;
                } else {
                    if (switchThisTime) {
                        k = toSwitchWithKey;
                        v = toSwitchWithValue;
                        switchThisTime = false;
                    }
                }

                playlist.set(k, v);

            }
            this.populateFrontend();
        }
    }

    downOne = (item) => {
        item = $(item);
        toRemove.clear();
        let artist = item.parents('li').attr('data-artist');
        let song = playlist.get(artist);
        let lastOneKey = null;
        let lastOneValue = null;
        let toSwitchWithKey = null;
        let toSwitchWithValue = null

        playlist.forEach(function (value, key) {
            if (lastOneKey === artist) {
                toSwitchWithKey = key;
                toSwitchWithValue = value;
            }
            lastOneKey = key;
            lastOneValue = value;
        });
        if (toSwitchWithKey) {
            let keys = Array.from(playlist.keys());
            let values = Array.from(playlist.values());
            let switchThisTime = false;

            playlist.clear();

            for (let i = 0; i < keys.length; i++) {

                let k = keys[i];
                let v = values[i];
                if (k === artist) {
                    k = toSwitchWithKey;
                    v = toSwitchWithValue;
                    switchThisTime = true;
                } else {
                    if (switchThisTime) {
                        k = artist;
                        v = song;
                        switchThisTime = false;
                    }
                }

                playlist.set(k, v);

            }
            this.populateFrontend();
        }
    }

    generatePlaylistFile = () => {
        this.exportPlaylist();
    }

    removeAndRefill = () => {
        toRemove.forEach(function (value, key) {
            playlist.delete(key);
        });

        playlist = this.recurse(playlist.size, artists, playlist);

        this.populateFrontend();
        toRemove.clear();
    }

    windowPlayCommand = (path) => {
        return '"\\Program Files (x86)\\MusicBee\\MusicBee.exe" /Play "' + path + '"';
    }

    windowStopCommand = () => {
        return 'taskkill /im MusicBee.exe';
    }

    getNextSong = (song) => {
        let next = false;
        let nextSong = '';
        playlist.forEach((value, key) => {
            if (next) {
                nextSong = value.song;
                next = false;
            } else {
                if (value.song === song) {
                    next = true;
                }
            }
        });

        return nextSong;
    }

    soundplay = async (path) => {
        let play = this;
        let chunks = path.split('\\');
        let newArtist = [chunks[0], chunks[1], chunks[2]].join('\\') + '\\';
        let sound = playlist.get(newArtist);
        Howler.stop();
        $('li').removeClass('playing');
        
        let li = $('li[data-song="' + path.replace(/\\/g, "\\\\")
           .replace(/\$/g, "\\$")
           .replace(/'/g, "\\'")
           .replace(/"/g, "\\\"") + '"]');

        li.addClass('playing');

        let nextSong = play.getNextSong(path);

        sound.play();
        sound.player.on('end', function(){
            if (nextSong) {
                play.soundplay(nextSong);
            } else {
                li.removeClass('playing');
            }
        });

    }

    playOnChromecast = async (song) => {
        let cast = this;
        let found = false;
        song = song || playlist.get(playlist.keys().next().value).song.split('\\Music\\')[1];
        client.devices.forEach(function(device) {
            if(device.name === $('#devices').val() && !found) {
                const media = {
                    url: 'http://192.168.1.76:3000/music/' + song.split('\\').join('/').split("#").join("%23"),
                    cover: {
                        title: 'HELLO THERE',
                        url: 'https://i.imgur.com/ysGa3As.jpg'
                    }
                }

 
                device.play(media, function (err) {
                    if (err) console.log(err);
                });



                device.on('finished', function() {
                    let nextSong = cast.getNextSong(directoryPath + song);
                    if (nextSong) {
                        cast.playOnChromecast(nextSong.split(directoryPath)[1]);
                    }
                });

                found = true;
            }
       })
    }

    soundstop = async (path) => {
        $('li').removeClass('playing');
        Howler.stop();
    }

    walkSync = (dir, filelist) => {
        let walksync = this;
        let files = fs.readdirSync(dir);
        filelist = filelist || [];

        files.forEach(function (file) {
            if (fs.statSync(dir + file).isDirectory()) {
                filelist = walksync.walkSync(dir + file + '\\', filelist);
            } else {
                if (path.extname(file) === '.mp3' || path.extname(file) === '.flac') {
                    filelist.push(dir + file);
                }
            }
        });
        return filelist;
    };

    recurse = (length, artists, playlist) => {
        if (!length) {
            length = 0;
        }

        if (length > 35) {
            return playlist;
        }

        let randomArtist = artists[Math.floor(Math.random() * artists.length)];

        if (randomArtist && !playlist.has(randomArtist)) {

            let artistSongs = this.walkSync(randomArtist, []);
            let randomSong = artistSongs[Math.floor(Math.random() * artistSongs.length)];

            if (randomSong) {
                playlist.set(randomArtist, new PlaylistItem(randomSong, randomArtist, false, false));
                length++;
            }
        }
        return this.recurse(length, artists, playlist);

    }

    putPlaylistFiles = () => {
        const time = Date.now();
        const finalDir = playlistDir + "final\\";
        let thisPlaylistDir = finalDir + time + "_playlist\\";
        let thisPlaylistFile = thisPlaylistDir + time + '_playlist.wpl';
        fs.mkdir(thisPlaylistDir, function (err) {
            if (err) {
                console.log(err)
            } else {
                let count = 0;
                playlist.forEach((value, key) => {
                    let countString = count + "";
                    if (value && value.song) {
                        fs.copyFile(value.song, finalDir + time + "_playlist\\" + countString.padStart(3, '0') + "_" + path.basename(value.song), (err) => {
                            if (err) throw err;
                        });
                    }
                    count++;
                });
                fs.copyFile(currentFile, thisPlaylistFile, (err) => {
                    if (err) throw err;
                });
            }
        })
    }

    exportPlaylist = (isCurrent) => {
        const time = Date.now();
        let exportList = this;
        const top = '<?wpl version="1.0"?>\n' +
            '<smil>\n' +
            '    <head>\n' +
            '        <meta name="Generator" content="Microsoft Windows Media Player -- 12.0.18362.900"/>\n' +
            '        <meta name="ItemCount" content="1"/>\n' +
            '        <title>test</title>\n' +
            '    </head>\n' +
            '    <body>\n' +
            '        <seq>\n';
        const bottom = '        </seq>\n' +
            '    </body>\n' +
            '</smil>';

        let lines = '';
        let thisPlaylistDir = playlistDir + time + "_playlist\\";
        let thisPlaylistFile = thisPlaylistDir + time + '_playlist.wpl';

        if (isCurrent) {
            thisPlaylistFile = currentFile;
        }


        playlist.forEach((value, key) => {
            if (value && value.song) {
                lines += '            <media needsEdit="' + value.needsEdit + '" approved="' + value.approved + '" src="' + XmlEntities.encode(value.song) + '"/>\n';
            }
        });
        
        if (!isCurrent) {
            fs.mkdirSync(thisPlaylistDir);
            fs.writeFile(thisPlaylistFile, top + lines + bottom, 'utf-8', function (err) {
                if (err) {
                    return console.log(err);
                }
                //exportList.soundplay(thisPlaylistFile);
            });
        } else {
            fs.writeFile(thisPlaylistFile, top + lines + bottom, 'utf-8', function (err) {
                if (err) {
                    return console.log(err);
                }
            });
        }
    }

    getArtists = () => {

        return new Promise((resolve, reject) => {
            fs.readdir(directoryPath, function (err, files) {
                if (err) {
                    reject(err);
                }

                files.forEach(function (file) {
                    if (fs.statSync(directoryPath + file).isDirectory()) {
                        artists.push(directoryPath + file + '\\');
                    }
                });

                resolve();
            });
        });
    }

    getNewPlaylist = () => {

        client.on('device', function(device){
            $('#devices').append('<option value="' + device.name + '" >' + device.friendlyName + '</option>');
        });

        // setTimeout(function() {
        //     client.devices.forEach(function(device) {
        //         console.log(device);
        //         $('#devices').append('<option value="' + device.name + '" >' + device.friendlyName + '</option>');
        //     });
        // }, 3000)


        let getNew = this;
        playlist = new Map();

        fs.stat(currentFile, function (error, stats) {
            if (!error && stats.isFile()) {
                getNew.loadFromFile(currentFile);
                if (!artists.length) {
                    getNew.getArtists();
                }
            } else {
                if (!artists.length) {
                    getNew.getArtists().then(() => {
                        playlist = getNew.recurse(0, artists, playlist);
                        getNew.populateFrontend();
                    });
                } else {
                    playlist = getNew.recurse(0, artists, playlist);
                    getNew.populateFrontend();
                }
            }
        });
    }

    loadSongFromPC = (item) => {
        item = $(item);
        let artist = item.parents('li').attr('data-artist');
        let loadSong = this;
        toRemove.clear();

        let response = dialog.showOpenDialog({
            properties: ['openFile'],
            defaultPath: artist,
            filters: [
                {name: 'Audio File', extensions: ['mp3', 'flac']}
            ]
        });

        response.then((promise) => {
            playlist.delete(artist);
            if (promise.filePaths.length) {
                let file = promise.filePaths[0];
                let chunks = file.split('\\');
                let newArtist = [chunks[0], chunks[1], chunks[2]].join('\\') + '\\';
                playlist.set(newArtist, new PlaylistItem(file, newArtist, false, false));
                loadSong.populateFrontend();
            }
        });
    }

    approve = (item) => {
        item = $(item);
        let artist = item.parents('li').attr('data-artist');
        if (item.hasClass('true')) {
            item.removeClass('true');
            if (playlist.get(artist))  {
                playlist.get(artist).approved = false;
            }
        } else {
            item.addClass('true');
            if (playlist.get(artist))  {
                playlist.get(artist).approved = true;
            }
        }

        this.populateFrontend();
    }

    needsEdit = (item) => {
        item = $(item);
        let artist = item.parents('li').attr('data-artist');
        if (item.hasClass('true')) {
            item.removeClass('true');
            if (playlist.get(artist))  {
                playlist.get(artist).needsEdit = false;
            }
        } else {
            item.addClass('true');
            if (playlist.get(artist))  {
                playlist.get(artist).needsEdit = true;
            }
        }

        this.populateFrontend();
    }

    populateFrontend = () => {
        $('#playlist').empty();
        toRemove.clear();
        let popIt = this;
        playlist.forEach(function (value, key) {
            let checkbox = '<div onclick="approve(this)" class="approved"></div>';
            if (value.approved) {
                checkbox = '<div onclick="approve(this)" class="approved true"></div>';
            }

            let needsEdit = '<div onclick="needsEdit(this)" class="needs-edit"></div>';
            if (value.needsEdit) {
                needsEdit = '<div onclick="needsEdit(this)" class="needs-edit true"></div>';
            }

            $('#playlist').append('<li data-artist="' + key + '" data-song="' + value.song + '"><div class="controls">' +
                '<button class="play control-button round" onclick="play(this)"></button>' +
                '<button class="another control-button round" onclick="anotherFromThisArtist(this)"></button>' +
                '<button class="another from-pc control-button round" onclick="loadSongFromPC(this)" ></button>' +
                '<button class="up-arrow control-button" onclick="upOne(this)"></button>' +
                '<button class="down-arrow control-button" onclick="downOne(this)"></button></div>' +
                '<div class="song-title" onClick="pick(this)"><span>' + value.song + '</span></div>' + checkbox + needsEdit +
                '</li>');
        })
        this.exportPlaylist(true);

    }

    load(track) {
        let response = dialog.showOpenDialog({
            properties: ['openFile'],
            filters: [
                {name: 'Windows Playlist', extensions: ['wpl']}
            ]
        });

        response.then((promise) => {
            if (promise.filePaths.length) {
                this.loadFromFile(promise.filePaths[0]);
            }
        });
    }

    loadFromFile = (filePath) => {

        let load = this;
        let xmlString = fs.readFileSync(filePath, "utf8");
        parser.parseString(xmlString.replace('/\\/g', '\\\\'), function (error, result) {
            if (error === null) {
                let list = result.smil.body[0].seq[0].media;
                playlist.clear();
                list.forEach(function (song) {
                    let chunks = song.ATTR.src.split('\\');
                    let artist = ([chunks[0], chunks[1], chunks[2]].join('\\') + '\\').replace('/\\/g', '\\\\');
                    let newSong = song.ATTR.src.replace('/\\/g', '\\\\');
                    let approved = song.ATTR.approved === 'true';
                    let needsEdit = song.ATTR.needsEdit === 'true';
                    playlist.set(artist, new PlaylistItem(newSong, artist, approved, needsEdit));
                    load.populateFrontend();
                });

            } else {
                console.log(error);
            }
        });
    }

    updateHowManyRecent = () => {
        const update = playlist;
        let dirs = [];
        fs.readdirSync(finalDir).forEach(file => {
          fs.stat(finalDir + file, function(err, stats) {
            if (err) console.log(err);
            if (stats.isDirectory()) {
                dirs.push(file);
            }
          });
        });

        dirs.sort();
        setTimeout(function() {

            let lastFive = [dirs.pop(), dirs.pop(), dirs.pop(), dirs.pop()];
            let five = dirs.pop();
            if (five) {
                lastFive.push(five);
            }

            let found = false;

            let allArtists = [];

            lastFive.forEach((it) => {
                // let load = this;
                let xmlString = fs.readFileSync(finalDir + it + "\\" + it + ".wpl", "utf8");
                parser.parseString(xmlString.replace('/\\/g', '\\\\'), function (error, result) {
                    if (error === null) {
                        let list = result.smil.body[0].seq[0].media;
                        list.forEach(function (song) {
                            let chunks = song.ATTR.src.split('\\');
                            allArtists.push(([chunks[0], chunks[1], chunks[2]].join('\\') + '\\').replace('/\\/g', '\\\\'));
                        });

                    } else {
                        console.log(error);
                    }
                });
            });
            setTimeout(function() {
                let count = 0;
                update.forEach((item, index) => {
                    if (allArtists.indexOf(index) !== -1) {
                        $('li:eq(' + count + ')').addClass('has-been');
                    }
                    count++;
                });
            }, 300);



        }, 300)


    }

    eraseCurrent() {
        fs.unlinkSync(currentFile);
    }
}